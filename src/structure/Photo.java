package structure;

import java.util.ArrayList;

public class Photo {
	private int id;
	private Position position;
	private ArrayList<String> tags;
	
	public Photo(int id, Position p, ArrayList<String> tags) {
		this.id = id;
		this.position = p;
		this.tags = tags;
	}
	
	public int getId() {
		return id;
	}

	public Position getPosition() {
		return position;
	}

	public ArrayList<String> getTags() {
		return tags;
	}
	
	public String toString() {
		String res = "";
		res+= "id: "+id+"\n";
		res+="position: "+position+"\n";
		res+="tags: "+tags+"\n";
		return res;
	}

}
