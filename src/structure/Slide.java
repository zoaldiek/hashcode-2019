package structure;

import java.util.ArrayList;

import tools.Tools;


public class Slide {
	private ArrayList<Photo> photos;
	
	public Slide(ArrayList<Photo> photos) {
		this.setPhotos(photos);
	}
	
	public ArrayList<String> getTags(){
		ArrayList<String> tags = new ArrayList<>(photos.get(0).getTags());
		for(int i = 1; i<photos.size();i++) {
			tags.removeAll(photos.get(i).getTags());
			tags.addAll(photos.get(i).getTags());
		}
		return tags;
	}
	
	public int getScore(Slide s) {
		int n1 = Tools.intersection(this.getTags(), s.getTags()).size();
		int n2 = this.getTags().size() - n1;
		int n3 = s.getTags().size() - n1;
		
		return Math.min(Math.min(n1, n2), n3);
	}

	public ArrayList<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(ArrayList<Photo> photos) {
		this.photos = photos;
	}

}
