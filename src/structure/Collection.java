package structure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import tools.Tools;

public class Collection {
	private int nbPhoto;
	private ArrayList<Photo> photos;

	private ArrayList<Slide> slides;

	public Collection(ArrayList<Photo> photos) {
		this.photos = photos;
		this.nbPhoto = this.photos.size();
		this.setSlides(null);
	}

	public void simpleGlouton() {
		ArrayList<Slide> vert = association1Verticale();

	}

	public void algoSwap() {
		ArrayList<Slide> slides;
		if (this.slides == null) {
			slides = association1Verticale();
			slides.addAll(getHorizontaleSlides());

			Collections.shuffle(slides);
		}
		else {
			slides = this.slides;
		}

		int total = getScore(slides);
		System.out.println("score1 = " + total+" len= "+slides.size());

		int score = -1;
		int cpt = 0;
		int cpt2 = 0;
		while (score != 0 || cpt < 1000) {
			score = 0;
			for (int i = 0; i < slides.size(); i++) {
				for (int j = 0; j < slides.size(); j++) {
					if (i != j) {
						int score1 = 0;
						int score2 = 0;
						
						if (i > 0) {
							score1 += slides.get(i).getScore(slides.get(i - 1));
						}
						if (i < 0) {
							score1 += slides.get(i).getScore(slides.get(i + 1));
						}
						if (j > 0) {
							score1 += slides.get(j).getScore(slides.get(j - 1));
						}
						if (j < 0) {
							score1 += slides.get(j).getScore(slides.get(j + 1));
						}
						
						slides = swap(slides, i, j);
						
						if (i > 0) {
							score2 += slides.get(i).getScore(slides.get(i - 1));
						}
						if (i < 0) {
							score2 += slides.get(i).getScore(slides.get(i + 1));
						}
						if (j > 0) {
							score2 += slides.get(j).getScore(slides.get(j - 1));
						}
						if (j < 0) {
							score2 += slides.get(j).getScore(slides.get(j + 1));
						}
						
						if (score2 > score1) {
							score = score2 - score1;
							total += score;
							System.out.println("score =   "+cpt2);
							this.slides = slides;
							cpt = 0;
							cpt2++;
							if(cpt2 == 1000) {
								cpt2 = 0;
								System.err.println("coucou team baker");
								Tools.writeFile("testb.txt", this);
							}
							break;
						}
						else {
							if(score2 != score1) {
								slides = swap(slides, j, i);
							}
							else {
								if(score2 == score1 && Math.random() < 0.5) {
									slides = swap(slides, j, i);
								}
							}
							
						}
					}

				}
			}
			cpt++;
			

		}this.slides=slides;

	}

	public ArrayList<Slide> swap(ArrayList<Slide> s, int i, int j) {
		Collections.swap(s, i, j);
		return s;
	}

	/**
	 * Prend le plus grand et le plus petit nombre de tag et les associ ensemble
	 * 
	 * @return
	 */
	public ArrayList<Slide> association1Verticale() {
		ArrayList<Slide> slidev = new ArrayList<>();
		ArrayList<Photo> listver = getVerticalPhotos();
		Collections.sort(listver, new PhotoComparator());
		for (int i = 0; i < listver.size() / 2; i++) {
			ArrayList<Photo> temp = new ArrayList<>();
			temp.add(listver.get(i));
			temp.add(listver.get(listver.size() - 1 - i));
			slidev.add(new Slide(temp));
		}
		return slidev;
	}

	public int getNbPhoto() {
		return nbPhoto;
	}

	public ArrayList<Photo> getVerticalPhotos() {
		ArrayList<Photo> res = new ArrayList<>();
		for (int i = 0; i < this.photos.size(); i++) {
			if (this.photos.get(i).getPosition() == Position.Vertical) {
				res.add(this.photos.get(i));
			}
		}
		return res;
	}

	public ArrayList<Photo> getHorizontalPhotos() {
		ArrayList<Photo> res = new ArrayList<>();
		for (int i = 0; i < this.photos.size(); i++) {
			if (this.photos.get(i).getPosition() == Position.Horizontal) {
				res.add(this.photos.get(i));
			}
		}
		return res;
	}

	public ArrayList<Slide> getHorizontaleSlides() {
		ArrayList<Slide> slideH = new ArrayList<>();
		ArrayList<Photo> photoH = getHorizontalPhotos();
		for (int i = 0; i < photoH.size(); i++) {
			ArrayList<Photo> photos = new ArrayList<>();
			photos.add(photoH.get(i));
			slideH.add(new Slide(photos));
		}
		return slideH;
	}

	public ArrayList<Photo> getPhotos() {
		return photos;
	}

	public ArrayList<Slide> getSlides() {
		return slides;
	}

	public void setSlides(ArrayList<Slide> slides) {
		this.slides = slides;
	}

	public int getScore(ArrayList<Slide> slides) {
		int score = 0;
		for (int i = 0; i < slides.size() - 1; i++) {
			score += slides.get(i).getScore(slides.get(i + 1));
		}
		return score;
	}

	public String toString() {
		String res = "";
		res += "----------------------------------------------------\n";
		res += "COLLECTION\n";
		res += "----------------------------------------------------\n\n";
		res += "Nombre de photos: " + nbPhoto + "\n\n";
		for (int i = 0; i < nbPhoto; i++) {
			res += photos.get(i) + "\n";
		}
		return res;
	}

}
