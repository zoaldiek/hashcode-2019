package structure;

import java.util.Comparator;

public class PhotoComparator implements Comparator<Photo> {

	@Override
	public int compare(Photo o1, Photo o2) {
		return Integer.compare(o1.getTags().size(), o2.getTags().size());
	}

}
