package tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import structure.Collection;
import structure.Photo;
import structure.Position;
import structure.Slide;

public class Tools {
	public static Collection readFile(String path) throws IOException {
		ArrayList<Photo> list = new ArrayList<>();
		File file = new File(path);

		BufferedReader br = new BufferedReader(new FileReader(file));

		String st;
		int compt = 0;
		while ((st = br.readLine()) != null) {
			if (compt != 0) {
				String[] compo = st.split(" ");
				ArrayList<String> tags = new ArrayList<>();
				for (int i = 0; i < Integer.parseInt(compo[1]); i++) {
					tags.add(compo[i + 2]);
				}
				if (compo[0].compareTo("H") == 0) {
					list.add(new Photo(compt - 1, Position.Horizontal, tags));
				} else {
					list.add(new Photo(compt - 1, Position.Vertical, tags));
				}

			}
			compt++;
		}
		br.close();

		return new Collection(list);

	}

	public static void writeFile(String path, Collection collection) {
		File f = new File(path);
		if (!f.exists() && !f.isDirectory()) {
			try {
				PrintStream out = new PrintStream(new FileOutputStream(path));
				out.println(collection.getSlides().size());
				System.out.println("enr size= " + collection.getSlides().size());
				for (int i = 0; i < collection.getSlides().size(); i++) {

					String line = "";
					for (int j = 0; j < collection.getSlides().get(i).getPhotos().size(); j++) {
						line += collection.getSlides().get(i).getPhotos().get(j).getId() + " ";
					}
					out.println(line);
				}
				out.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	public static <T> List<T> intersection(List<T> list1, List<T> list2) {
		List<T> list = new ArrayList<T>();

		for (T t : list1) {
			if (list2.contains(t)) {
				list.add(t);
			}
		}

		return list;
	}

	public static void solutionReadFile(String path, Collection c) throws NumberFormatException, IOException {
		File file = new File(path);

		BufferedReader br = new BufferedReader(new FileReader(file));

		String st;
		int compt = 0;
		ArrayList<Slide> slides = new ArrayList<>();
		while ((st = br.readLine()) != null) {
			if (compt != 0) {
				String[] compo = st.split(" ");
				ArrayList<Photo> photos = new ArrayList<>();
				for (int i = 0; i < compo.length; i++) {
					photos.add(c.getPhotos().get(Integer.parseInt(compo[i])));
				}
				slides.add(new Slide(photos));
			}
			compt++;
		}
		c.setSlides(slides);
		br.close();

	}

	/**
	 * 
	 * Attention l'ordre de la liste est importante
	 * 
	 * @param list1
	 * @param list2
	 * @return
	 */
	public static <T> List<T> différence(List<T> list1, List<T> list2) {
		List<T> list = new ArrayList<T>();

		for (T t : list1) {
			if (!list2.contains(t)) {
				list.add(t);
			}
		}

		return list;

	}

}
