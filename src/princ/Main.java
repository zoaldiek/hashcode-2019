package princ;

import java.io.IOException;

import structure.Collection;
import tools.Tools;

public class Main {
	
	public static final String path = "ressources/input/b_lovely_landscapes.txt";

	public static void main(String[] args) throws IOException {
		Collection collection = Tools.readFile(path);
		Tools.solutionReadFile("ressources/b_lovely_landscapes.txt", collection);
		collection.algoSwap();
		Tools.writeFile("ressources/output/b_lovely_landscapes.txt", collection);
	}

}
